/**
 * Created by richardbrash on 1/2/15.
 */

var express = require('express');
var xmlrpc = require('xmlrpc');
var router = express.Router();
var async = require('async');



function InfusionsoftApiClient(){

    this.Caller = function(clientid, service, input, callback){

        //  Get the config for the clientid passed in
        Config.findOne( { '_id':clientid } )
            .exec(function(err, clientConfig){
                if(!err){

                    var client = xmlrpc.createSecureClient('https://' + clientConfig.config.appName + '.infusionsoft.com/api/xmlrpc');

                    var data = [clientConfig.config.apiKey].concat(input);

                    client.methodCall(service, data, function(error, value){
                        if(!error){
                            callback(null, value);

                        } else {
                            callback(error, null);
                        }
                    });
                }
            });


    }

}


/* GET stageload (does work. */
router.get('/', function(req, res) {

    var sourceApiKey = "65b124a91a4568f732b8b9d58900dd05";
    var targetApiKey = "c4d846c56b7225233f13be4ee9f4143e";
    var sourceClient = xmlrpc.createSecureClient('https://' + 'midcomdata' + '.infusionsoft.com/api/xmlrpc');
    var targetClient = xmlrpc.createSecureClient('https://' + 'yq173' + '.infusionsoft.com/api/xmlrpc');


    sourceClient.methodCall("DataService.query", [sourceApiKey, "Stage", 1000, 0, {}, ["StageName", "StageOrder", "TargetNumDays"] ], function(error, value){

        async.forEachLimit(value, 2, function(c, callback){

            targetClient.methodCall("DataService.add", [targetApiKey, "Stage", c ], function(error, value){

                if(error){
                    console.log(error);
                } else {
                    console.log(value);
                }

                callback();

            });


        });


    });


    res.json({Message:"Test"});

});

module.exports = router;
