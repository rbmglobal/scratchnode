/**
 * Created by richardbrash on 1/6/15.
 */
var express = require('express');
var InfusionsoftApiClient = require('../lib/InfusionsoftApiClient');

var router = express.Router();

router.param('id', function(req, res, next, id){
    req.id = id;
    next();

});


/* GET quotes listing. */
router.get('/', function(req, res) {
    res.json({message:"Idiot!"});
});


router.get('/:id', function(req, res) {

    InfusionsoftApiClient.Caller("ContactService.load", [req.id, ["Id", "CompanyID"]], function(error, value){
        if(error){
            console.log(error);
        } else {
            res.json(value);
        }

    });

});


/* GET quotes listing. */
router.post('/', function(req, res) {

    var input = req.body;
    var cid = input.Id;

    //  First, we want to get the CompanyID
    InfusionsoftApiClient.Caller("ContactService.load", [cid, ["Id", "CompanyID"]], function(error, workitem){

        if(error){
            res.json(error)
        } else {

            //  First, get the Company Record so we can get the data required to update the contact record withg
            //InfusionsoftApiClient.Caller("DataService.load", ["Company", workitem.CompanyID, ["Id", "_AnnualRevenue", "_NumberofEmployees", "Company", "StreetAddress1", "City", "State", "PostalCode"]], function(error, company){
            //InfusionsoftApiClient.Caller("DataService.load", ["Company", workitem.CompanyID, ["Id", "_NumberofEmployees", "Company", "StreetAddress1", "City", "State", "PostalCode"]], function(error, company){
            InfusionsoftApiClient.Caller("DataService.load", ["Company", workitem.CompanyID, ["Id", "_Employees", "_Revenue", "_YearEstablished", "_NAICS","_SIC", "_CompanyDescription", "Company"]], function(error, company){
                if(error){
                    res.json(error)
                } else {
                    //  Now, build up the data packet for the Contact update
                    var data = {
                        _CompanyName:company.Company,
                        _NumberofEmployees: company._Employees,  //TEXT
                        _AnnualRevenue: company._Revenue,  // CURRENCY
                        _YearEstablished: company._YearEstablished,  //DATE
                        _NAICS: company._NAICS,  // TEXT
                        _SIC: company._SIC, //TEXT
                        _CompanyDescription: company._CompanyDescription,  //TEXT AREA
                        //_AnnualRevenue: company._AnnualRevenue,  //  UPDATE CUSTOM FIELD NAMES TO MATCH APP
                        ContactType: "Prospect",
                        CompanyID: 12818  // This is Whitaker Partners

                    }

                    //  Update the Contact
                    InfusionsoftApiClient.Caller("ContactService.update", [workitem.Id, data], function(error, update){
                        if(error){
                            res.json(error)
                        }else {
                            if(update){
                                //  Finally, we can delete the original Company record
                                InfusionsoftApiClient.Caller("DataService.delete", ["Company", company.Id], function(error, del){

                                    if(error){
                                        res.json(error)
                                    } else {
                                        res.json({message:del});
                                    }
                                });
                            } else {
                                res.json({message:"Not deleted!"});
                            }

                        }

                    });
                }
            });


        }

    });




});


module.exports = router;



