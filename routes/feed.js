var express = require('express');
var router = express.Router();
var xml = require('xml');


var htmlencode = require('htmlencode').htmlEncode;



/* GET home page. */
router.get('/', function(req, res) {

    ///* lets create an rss feed */
    //var feed = new RSS({
    //    title: 'title',
    //    description: 'description',
    //    feed_url: 'http://example.com/rss.xml',
    //    site_url: 'http://example.com',
    //    image_url: 'http://example.com/icon.png',
    //    docs: 'http://example.com/rss/docs.html',
    //    managingEditor: 'Dylan Greene',
    //    webMaster: 'Dylan Greene',
    //    copyright: '2013 Dylan Greene',
    //    language: 'en',
    //    categories: ['Category 1','Category 2','Category 3'],
    //    pubDate: 'May 20, 2012 04:00:00 GMT',
    //    ttl: '60',
    //    customNamespaces: {
    //        'itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd'
    //    },
    //    custom: [
    //        {'itunes:subtitle': 'A show about everything'},
    //        {'itunes:author': 'John Doe'},
    //        {'itunes:summary': 'All About Everything is a show about everything. Each week we dive into any subject known to man and talk about it as much as we can. Look for our podcast in the Podcasts app or in the iTunes Store'},
    //        {'itunes:owner': [
    //            {'itunes:name': 'John Doe'},
    //            {'itunes:email': 'john.doe@example.com'}
    //        ]},
    //        {'itunes:image': {
    //            _attr: {
    //                href: 'http://example.com/podcasts/everything/AllAboutEverything.jpg'
    //            }
    //        }},
    //        {'itunes:category': [
    //            {_attr: {
    //                text: 'Technology'
    //            }},
    //            {'itunes:category': {
    //                _attr: {
    //                    text: 'Gadgets'
    //                }
    //            }}
    //        ]}
    //    ]
    //});
    //
    ///* loop over data and add to feed */
    //feed.item({
    //    title:  'item title',
    //    description: htmlencode('<table><tr><td>c1r1</td><td>c2r1</td></tr><tr><td>c1r2</td><td>c2r2</td></tr><tr><td>c1r3</td><td>c2r3</td></tr></table>'),
    //    url: 'http://example.com/article4?this&that', // link to the item
    //    guid: '1123', // optional - defaults to url
    //    categories: ['Category 1','Category 2','Category 3','Category 4'], // optional - array of item categories
    //    author: 'Guest Author', // optional - defaults to feed author property
    //    date: 'May 27, 2012', // any format that js Date can parse.
    //    lat: 33.417974, //optional latitude field for GeoRSS
    //    long: -111.933231, //optional longitude field for GeoRSS
    //    custom: [
    //        {'itunes:author': 'John Doe'},
    //        {'itunes:subtitle': 'A short primer on table spices'},
    //        {'itunes:image': {
    //            _attr: {
    //                href: 'http://example.com/podcasts/everything/AllAboutEverything/Episode1.jpg'
    //            }
    //        }},
    //        {'itunes:duration': '7:04'}
    //    ]
    //});
    var xmlobj = {
        rss:{
            channel:{
            title:"My RSS Feed",
            description:"My Feed Description",
            link: "http://example.com",
            language: "en"
            //item:{
            //    title:"Title 1",
            //    description:"This is an Example",
            //    link:"http://example.com/123"
            //},
            //item:{
            //    title:"Title 2",
            //    description:"This is an Example",
            //    link:"http://example.com/123"
            //},
            //    item:{
            //        title:"Title 3",
            //        description:"This is an Example",
            //        link:"http://example.com/123"
            //    }
            }
        }
    };


// cache the xml to send to clients
    var data = xml(xmlobj, { declaration: true });

    res.header('Content-Type','text/xml').send(data);



});

module.exports = router;
