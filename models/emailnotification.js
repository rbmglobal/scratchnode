/**
 * Created by richardbrash on 1/6/15.
 */
/**
 * Created by richardbrash on 12/5/14.
 */
// Load required packages
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define our client schema
var EmailNotificationSchema = new mongoose.Schema({

    message:{ type: String, required: true }

    //account_id:{ type: String, required: true },
    //webhook_id:{type:String, required:true},
    //timestamp:{type:Number, required:true},
    //message_data:{
    //    message_id:{type:String, required:true},
    //    email_message_id:{type:String, required:true},
    //    person_info:{type:Object, required:true},
    //    references:{type:Array, required:true},
    //    date:{type:Number, required:true},
    //    subject:{type:String, required:true},
    //    folders:{type:Array, required:true}
    //},
    //token:{type:String, required:true},
    //signature:{type:String, required:true}


});

module.exports = mongoose.model('EmailNotification', EmailNotificationSchema);